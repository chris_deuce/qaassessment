'use strict';
var homePage = function(){
	this.signIn = element(by.id('sign_in'));
	const URL = "https://qa-test.avenuecode.com"

	this.getHomePage = function () {
		browser.get(URL);
	}

	this.enterSignIn = function(){
        this.signIn.click();
    };
};
module.exports = homePage;