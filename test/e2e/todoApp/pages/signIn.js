'use strict';
var signIn = function(){
	this.emailtx = element(by.id('user_email'));
	this.pwd = element(by.id('user_password'));
	this.button = element(by.css('input.btn.btn-primary'));

	this.enterEmail = function (email) {
		this.emailtx.sendKeys(email);
	};

	this.enterPwd = function (pwds) {
		this.pwd.sendKeys(pwds);
	};

	this.submit = function () {
		this.button.click();
	};

};
module.exports = signIn;