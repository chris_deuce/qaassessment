'use strict';
var tasks = function(){

    this.textBox = element(by.id('new_task'));
    //this.textBox = element(by.css('input.form-control.ng-valid-maxlength.ng-touched.ng-dirty.ng-valid-parse.ng-valid.ng-valid-required'));
    this.welcome = element.all(by.css('a')).get(2);
    this.add = element(by.css('span.input-group-addon.glyphicon.glyphicon-plus'));
    this.newTask = element(by.css('a.ng-scope.ng-binding.editable.editable-click'));

    this.enterTaskByPressingButton=function(taskA){
        this.textBox.click();
        this.textBox.sendKeys(taskA);
        this.add.click();
    }

    this.enterTask =function(taskA){
        this.textBox.sendKeys(taskA);
    }
};
module.exports = tasks;