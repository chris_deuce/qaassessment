'use strict';
var originalTimeout;
let home = require('../pages/homePage');
let sign = require ('../pages/signIn');
let user = require('../pages/userInst');
let task = require('../pages/tasks');

var nhome = new home();
var nsign = new sign();
var nUser = new user();
var ntask = new task();

beforeEach(function(){
	originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
	jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
})

afterEach(function () {
	jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
})

describe('A given user when logs into Todo App',function(){
	it('should have always see "My tasks"',function() {
		browser.driver.manage().window().maximize();
		browser.waitForAngularEnabled(false);
        nhome.getHomePage();
		browser.driver.sleep(5000);
		nhome.enterSignIn();
		browser.driver.sleep(500);
		nsign.enterEmail('felixruiz.mendoza@gmail.com');
		nsign.enterPwd('T0d0AppQA$S');
		nsign.submit();
		browser.driver.sleep(1000);
		expect(nUser.tasks).toBeDefined();
	})

	it('should redirect to created task when link is pressed',function() {
		// body...
		nUser.gotoTasks();
		browser.driver.sleep(1500);
		expect(ntask.welcome.getText()).toEqual('Welcome, Felix!');
	})

	it('should add a new tasks on the todo',function(){
		ntask.enterTaskByPressingButton('LinkApp');
		browser.driver.sleep(1000);
		//browser.actions().sendKeys(protractor.Key.ENTER).perform();
	})

});