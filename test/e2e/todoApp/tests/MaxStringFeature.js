var originalTimeout;
var home = require('../pages/homePage');
var sign = require('../pages/signIn');
var user = require('../pages/userInst');
var task = require('../pages/tasks');

var nhome = new home();
var nsign = new sign();
var nUser = new user();
var ntask = new task();

beforeEach(function(){
	originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
	jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
});

afterEach(function () {
	jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
})

describe('A given user when logs into Todo App',function(){
	it('should have always see "My tasks"',function() {
		browser.driver.manage().window().maximize();
		browser.waitForAngularEnabled(false);
		nhome.getHomePage();
		browser.driver.sleep(5000);
		nhome.enterSignIn();
		browser.driver.sleep(500);
		nsign.enterEmail('felixruiz.mendoza@gmail.com');
		nsign.enterPwd('T0d0AppQA$S');
		nsign.submit();
		browser.driver.sleep(1000);
		nUser.gotoTasks();
		browser.driver.sleep(500);
		ntask.enterTaskByPressingButton('A123213213njlkn4l1n23l213po232dm141pom41m1pom4odm43medmxpmsp4mp234p23m4o2m3pmd2pm34dm23p4dmp234mdp323md2p34dmo4md2p3o4md23o4md2po434omdop4m23pmexeopmdx2pm3o2pmdpo4323dmccmxmxslslsl´23mmeñ23mñmñ23l23lmñ2lmdñl32mdñl23mdñ2l3mdxcmbbb´dvdewfwfweffewe23ssss');
		browser.driver.sleep(300);
        expect(ntask.task).toBeUndefined();
	})

})	