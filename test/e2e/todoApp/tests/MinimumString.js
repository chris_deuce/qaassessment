var originalTimeout;
var home = require('../pages/homePage.js');
var sign = require('../pages/signIn.js');
var user = require('../pages/userInst.js');
var task = require('../pages/tasks.js');

var nhome = new home();
var nsign = new sign();
var nUser = new user();
var ntask = new task();

beforeEach(function(){
	originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
	jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
});

afterEach(function () {
	jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout; 
})

describe('A given user when logs into Todo App',function(){
	it('should not add a tasks if it has less than 3 chars',function() {
		browser.driver.manage().window().maximize();
		browser.waitForAngularEnabled(false);
		nhome.getHomePage();
		browser.driver.sleep(5000);
		nhome.enterSignIn();
		browser.driver.sleep(500);
		nsign.enterEmail('felixruiz.mendoza@gmail.com');
		nsign.enterPwd('T0d0AppQA$S');
		nsign.submit();
		browser.driver.sleep(1000);
		nUser.gotoTasks();
		browser.driver.sleep(1900);
		ntask.enterTaskByPressingButton('A22');
		browser.driver.sleep(900);
        expect(ntask.task).toBeUndefined();
	})

});