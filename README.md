# # QA Test Avenue
The is the scripts to test the first feature for the TodoApp

# Prerequisites
In order to build and run the application you'll need to install:
    1. Java JDK
    2. NodeJS
    3. Protractor

# Setup
Once inside the root project folder, execute:
    
    1. Install nodejs: get the installer from official site
    2. Install protractor: npm install protractor -g 
    3. Install jasmine: npm install -g jasmine
    4. Get webdriver modules: webdriver-manager update
    5. Start local webdriver to run it: webdriver-manager start ; use a console to run it
    6. Select the feature that you want, uncomment lines from 4 to 6 to run single test or by wilcarding *.js you will run all tests
    7. Run the test: protractor runtest.js
    8. In case you want to run other test case just update line 4 with the name of the test that you want to run i.e feature1.js
    9. Review the log to review if an spec had failed


# Run Locally
Modify the configuration file as needed

Locate the root project folder and open a console, type protractor runtest.js
