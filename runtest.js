exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    //specs: ['test/e2e/todoApp/tests/WelcomeMessage.js'],
    specs: ['test/e2e/todoApp/tests/MinimumString.js'],
    //specs: ['test/e2e/todoApp/tests/MaxStringFeature.js'],
    capabilities:{ browserName: 'chrome',
    //capabilities:{ browserName: 'firefox',
    //chromeOptions: {
    //  args: ["--headless","--disable-gpu","--window-size=800x600"]
    // }  
    },
    //capabilities:{ browserName: 'firefox'},
    jasmineNodeOpts: {
       realtimeFailure: true
    }
  };